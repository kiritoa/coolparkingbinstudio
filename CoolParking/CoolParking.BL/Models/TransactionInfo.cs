﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
	public struct TransactionInfo
	{
		/// <summary>
		/// Time when transaction has been executed
		/// </summary>
		public DateTime TransactionTime { get; private set; }
		
		/// <summary>
		/// Vehicle which balance has been updated
		/// </summary>
		public string VehicleId { get; private set; }
		
		/// <summary>
		/// Amount of units in transaction
		/// </summary>
		public decimal Sum { get; private set; }

		public TransactionInfo(string vehicleId, decimal sum)
		{
			this.TransactionTime = DateTime.Now;
			this.VehicleId = vehicleId;
			this.Sum = sum;
		}

		public string ToLogFormat() => $"[{TransactionTime}]: {VehicleId} --> {Sum}";
	}
}