﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
	public class Parking
	{
		private static Parking _instance;
		public static Parking Instance
		{
			get
			{
				if (_instance == null)
					_instance = new Parking();
				return _instance;
			}
		}
		public int Capacity { get; private set; }
		public decimal Balance { get; set; }
		public Dictionary<string, Vehicle> Vehicles { get; set; }
		private Parking()
		{
			Capacity = Settings.Capacity;
			Balance = Settings.StartBalance;
			Vehicles = new Dictionary<string, Vehicle>();
		}
	}
}