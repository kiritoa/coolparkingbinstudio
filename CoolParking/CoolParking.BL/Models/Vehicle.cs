﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public decimal Balance { get; internal set; }
        public string  Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        
        /// <param name="id">Vehicle Id, that has "WW-DDDD-WW" format</param>
        /// <exception cref="ArgumentException">throws ArgumentException if @id wasn`t formatted </exception>
        public Vehicle (string id, VehicleType vehicleType, decimal balance)
        {
            if(!Regex.IsMatch(input: id, pattern: Settings.VehicleIdTemplate))
                throw new ArgumentException($"Invalid Id given {id}");
            
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string result = "";
            Random rnd = new Random((int)(DateTime.Now.Ticks));

            result += GetNRandomChars(rnd, 2);
            result += '-';
            for (int i = 0; i < 4; i++)
                result += rnd.Next(10).ToString();
            result += '-';
            result += GetNRandomChars(rnd, 2);
            return result;
        }

        private static string GetNRandomChars(Random rnd, int n = 1)
        {
            // English alphabet +1 `cause including Z
            var alphabet = Enumerable.Range('A', 'Z' - 'A' + 1).ToArray();
            string res = "";

            for (int i = 0; i < n; i++)
                res += (char)rnd.Next(alphabet.First(), alphabet.Last());

            return res.ToString();
        }

        public override string ToString() => 
            $"\"{this.Id}\", Balance: {Balance} {Settings.VehicleTypeString[this.VehicleType]}";
    }
}