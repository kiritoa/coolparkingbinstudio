﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
	public static class Settings
	{
		public static string VehicleIdTemplate { get; } = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

		public static decimal StartBalance { get; } = 0;
		public static int Capacity { get; } = 10;

		/// <summary>
		/// Interval to get charge from vehicles 
		/// </summary>
		// seconds * 1000.0 => milliseconds for Intervals
		public static double PayInterval { get; } = 5 * 1000.0;

		public static double LogInterval { get; } = 60 * 1000.0;
		public static string LogFile { get; } = @"./Transactions.log";

		public static readonly Dictionary<VehicleType, decimal> VehicleTypeCharge
			= new Dictionary<VehicleType, decimal>()
			{
				{VehicleType.Motorcycle, 1M},
				{VehicleType.PassengerCar, 2M},
				{VehicleType.Bus, 3.5M},
				{VehicleType.Truck, 5M}
			};
		public static readonly Dictionary<VehicleType, string> VehicleTypeString
			= new Dictionary<VehicleType, string>()
			{
				{VehicleType.Motorcycle, "Motorcycle"},
				{VehicleType.PassengerCar, "PassengerCar"},
				{VehicleType.Bus, "Bus"},
				{VehicleType.Truck, "Truck"}
			};

		public static decimal FineCoefficient { get; } = 2.5M;
	}
}