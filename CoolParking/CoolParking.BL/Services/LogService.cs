﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
	public class LogService : ILogService
	{
		public string LogPath { get; private set; }

		public LogService(string logPath)
		{
			this.LogPath = logPath ?? Settings.LogFile;
		}
		
		public void Write(string logInfo)
		{
			StreamWriter file = new StreamWriter(LogPath, true, Encoding.UTF8);
			file.WriteLine(logInfo);
			file.Close();
		}
		
		public string Read()
		{
			StreamReader file;
			if (!File.Exists(LogPath))
				throw new InvalidOperationException($"File {LogPath} doesn`t exist");
			file = new StreamReader(LogPath, Encoding.UTF8);
			var res = file.ReadToEnd();
			file.Close();
			return res;
		}
	}
}