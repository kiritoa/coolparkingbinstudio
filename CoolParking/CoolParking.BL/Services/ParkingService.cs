﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Xml.Serialization;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Settings = CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Services
{
	public class ParkingService : IParkingService
	{
		private Parking _parking;
		private ITimerService _withdrawTimer;
		private ITimerService _logTimer;
		private ILogService _logService;
		private TransactionInfo[] _transactions;
		private void GetFee(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			try
			{
				foreach (Vehicle vehicle in this._parking.Vehicles.Values)
				{
					decimal charge = Settings.VehicleTypeCharge[vehicle.VehicleType];


					if (vehicle.Balance < charge)
					{
						//Could be ternary operator, but KISS
						if (vehicle.Balance < 0)
							charge *= Settings.FineCoefficient;
						else
							charge += (charge - vehicle.Balance) * Settings.FineCoefficient;
					}

					vehicle.Balance -= charge;
					_parking.Balance += charge;

					TransactionInfo transaction = new TransactionInfo(vehicle.Id, charge);
					_transactions = _transactions.Append(transaction).ToArray();
				}
			}
			catch (NullReferenceException)
			{
				return;
			}
		}

		private void LogTransactions(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			foreach (TransactionInfo transaction in _transactions)
				_logService.Write(transaction.ToLogFormat());
			_transactions = Array.Empty<TransactionInfo>();
			GC.Collect();
		}

		public ParkingService(
			ITimerService withdrawTimer,
			ITimerService logTimer,
			ILogService logService)
		{
			this._parking = Parking.Instance;
			this._withdrawTimer = withdrawTimer
							   ?? new TimerService(Settings.PayInterval, GetFee);
			this._logTimer = logTimer
						  ?? new TimerService(Settings.LogInterval, LogTransactions);
			this._logService = logService
							?? new LogService(Settings.LogFile);
			this._withdrawTimer.Start();
			this._transactions = Array.Empty<TransactionInfo>();
			this._logTimer.Start();
		}

		public void AddVehicle(Vehicle vehicle)
		{
			if (_parking.Capacity == _parking.Vehicles.Count)
				throw new InvalidOperationException("Parking is full!");
			try
			{
				this._parking.Vehicles.Add(vehicle.Id, vehicle);
			}
			catch (ArgumentException)
			{
				throw new ArgumentException("Vehicle is already on the parking");
			}
		}

		public void RemoveVehicle(string vehicleId)
		{
			if (!_parking.Vehicles.ContainsKey(vehicleId))
				throw new ArgumentException($"Vehicle with id [{vehicleId}] is not on the parking");
			if (_parking.Vehicles[vehicleId].Balance < 0)
				throw new InvalidOperationException("A vehicle with the negative balance you are trying to remove");
			_parking.Vehicles.Remove(vehicleId);
		}

		public decimal GetBalance() => _parking.Balance;

		public int GetCapacity() => _parking.Capacity;
		public int GetFreePlaces() => GetCapacity() - _parking.Vehicles.Count;

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			ReadOnlyCollection<Vehicle> res = new ReadOnlyCollection<Vehicle>(
				_parking.Vehicles.Values.ToList()
			);
			return res;
		}

		public void TopUpVehicle(string vehicleId, decimal sum)
		{
			if (!_parking.Vehicles.ContainsKey(vehicleId))
				throw new ArgumentException($"Vehicle with id [{vehicleId}] is not on the parking");
			if (sum <= 0)
				throw new ArgumentException($"You can`t top up a vehicle with {sum} units, wait for withdrawing");
			_parking.Vehicles[vehicleId].Balance += sum;
		}

		public TransactionInfo[] GetLastParkingTransactions() => _transactions;

		public string ReadFromLog() => _logService.Read();

		public void Dispose()
		{
			_logTimer.Dispose();
			_withdrawTimer.Dispose();
		}
	}
}