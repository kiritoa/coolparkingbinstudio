﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Diagnostics.Tracing;
using System.Threading;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Timer = System.Timers.Timer;

namespace CoolParking.BL.Services
{
	public class TimerService : ITimerService
	{
		public event ElapsedEventHandler Elapsed;
		public double Interval { get; set; }
		public Timer _timer;
		public TimerService(double interval, ElapsedEventHandler callback)
		{
			this.Interval = interval;
			_timer = new Timer(Interval);
			this.Elapsed += callback;
			_timer.Elapsed += callback;
		}
		public void Start()
		{
			_timer.Start();
		}
		public void Stop()
		{
			_timer.Stop();
		}

		public void Dispose()
		{
			_timer.Dispose();
			GC.Collect();
		}
	}
}